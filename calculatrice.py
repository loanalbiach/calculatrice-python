import tkinter as tk
import re
from datetime import datetime, timedelta

# Déclaration de variables globales
input_field = None
validate_button = None
return_button = None
result_label = None
binary_input = None
input_base_dropdown = None
output_base_dropdown = None
convert_label = None
en_label = None
all_buttons = None
lbl_start = None
entry_start = None
lbl_end = None
entry_end = None
principal_entry = None 
rate_entry = None
time_entry = None
periods_entry = None
payment_amount_entry = None
lbl_principal = None
lbl_rate = None
lbl_time = None
lbl_periods = None
lbl_payments = None
input_unit_dropdown = None
output_unit_dropdown = None
start_date_default = None
end_date_default = None
lbl_years = None
lbl_months = None
lbl_weeks = None
lbl_days = None
lbl_hours = None
lbl_minutes = None
lbl_seconds = None
label_poids = None
poids_entry = None
label_taille = None
taille_entry = None

# Fonction pour cacher tous les boutons
def hide_buttons(buttons):
    for button in buttons:
        button.pack_forget()

def show_calculators(window):
    # Liste de tous les éléments à cacher
    elements_to_hide = [binary_input, validate_button, return_button, result_label,
                        input_base_dropdown, output_base_dropdown, en_label,
                        convert_label, input_field, entry_start, entry_end, lbl_start, lbl_end, 
                        principal_entry, rate_entry, time_entry, periods_entry, payment_amount_entry, 
                        lbl_principal, lbl_rate, lbl_time, lbl_periods, lbl_payments, input_unit_dropdown, 
                        output_unit_dropdown, start_date_default, end_date_default, lbl_years, lbl_months, 
                        lbl_weeks, lbl_days, lbl_hours, lbl_minutes, lbl_seconds, label_poids, poids_entry,
                        label_taille, taille_entry]

    # Filtrer les widgets Tkinter de la liste
    widgets_to_hide = [widget for widget in elements_to_hide if isinstance(widget, tk.Widget)]

    # Cacher tous les éléments qui existent
    for widget in widgets_to_hide:
        widget.pack_forget()

    # Afficher les boutons des différentes calculatrices
    for button in all_buttons:
        button.pack(pady=5)



def open_binary_calculator(window, btn_style):
    global binary_input, input_base_dropdown, output_base_dropdown, convert_label, en_label, validate_button, return_button, result_label

    # Cacher les boutons des autres calculatrices
    hide_buttons(all_buttons)

    # Créer un widget d'entrée (input) pour les nombres binaires
    binary_input = tk.Entry(window, font=("Arial", 12), width=30)
    binary_input.pack(pady=10)

    # Créer le libellé "Convertir"
    convert_label = tk.Label(window, text="Convertir", font=("Arial", 12))
    convert_label.pack()

    # Créer une dropdown pour choisir la base d'entrée
    input_base_options = ['Base 2', 'Base 10', 'Base 16']
    input_base_var = tk.StringVar(window)
    input_base_var.set(input_base_options[0])  # Définir la valeur par défaut
    input_base_dropdown = tk.OptionMenu(window, input_base_var, *input_base_options)
    input_base_dropdown.pack()

    # Ajouter le libellé "en"
    en_label = tk.Label(window, text="en", font=("Arial", 12))
    en_label.pack()

    # Créer une dropdown pour choisir la base de conversion
    output_base_var = tk.StringVar(window)
    output_base_var.set(input_base_options[1])  # Définir la valeur par défaut
    output_base_dropdown = tk.OptionMenu(window, output_base_var, *input_base_options)
    output_base_dropdown.pack()

    # Créer une étiquette pour afficher le résultat
    result_label = tk.Label(window, text="", font=("Arial", 12))
    result_label.pack()

    # Définition de la fonction pour valider et calculer le nombre binaire
    def validate_and_calculate():
        binary_number = binary_input.get()
        input_base = input_base_var.get()
        output_base = output_base_var.get()

        # Vérifier si l'entrée est valide
        if not binary_number or not re.match(r'^[0-9A-Fa-f]+$', binary_number):
            # Afficher un message d'erreur si l'entrée est invalide
            result_label.config(text="Entrée invalide. Veuillez entrer un nombre valide.")
            return

        # Convertir le nombre en fonction de la base sélectionnée
        if input_base == 'Base 2':
            decimal_number = int(binary_number, 2)
        elif input_base == 'Base 10':
            decimal_number = int(binary_number, 10)
        elif input_base == 'Base 16':
            decimal_number = int(binary_number, 16)

        if output_base == 'Base 2':
            result_label.config(text=f"Le nombre {binary_number} en base {input_base[5:]} est équivalent à {bin(decimal_number)[2:]} en base 2.")
        elif output_base == 'Base 10':
            result_label.config(text=f"Le nombre {binary_number} en base {input_base[5:]} est équivalent à {decimal_number} en base 10.")
        elif output_base == 'Base 16':
            result_label.config(text=f"Le nombre {binary_number} en base {input_base[5:]} est équivalent à {hex(decimal_number)[2:].upper()} en base 16.")

    # Créer un bouton pour valider l'entrée et effectuer le calcul
    validate_button = tk.Button(window, text="Valider", command=validate_and_calculate, **btn_style)
    validate_button.pack(pady=5)

    # Créer un bouton "Retour" pour revenir à la sélection de calculatrices
    return_button = tk.Button(window, text="Retour", command=lambda: show_calculators(window), **btn_style)
    return_button.pack(pady=5)

    # Lier l'appui sur la touche Entrée à la fonction validate_and_calculate
    window.bind('<Return>', lambda event: validate_and_calculate())


def open_standard_calculator(window, btn_style):
    global input_field, validate_button, return_button, result_label

    # Cacher tous les boutons
    hide_buttons(all_buttons)

    # Créer un widget d'entrée (input)
    input_field = tk.Entry(window, font=("Arial", 12), width=30)
    input_field.pack(pady=10)

    # Fonction pour valider et évaluer l'expression mathématique
    def validate_and_evaluate():
        expression = input_field.get()

        # Vérifier si l'expression est valide
        if re.match(r'^[\d\s+\-*/.()]+$', expression):
            # Évaluer l'expression et afficher le résultat
            try:
                result = eval(expression)
                result_label.config(text="Résultat: " + str(result))
            except Exception as e:
                result_label.config(text="Erreur: " + str(e))
        else:
            result_label.config(text="Expression invalide")

    # Créer une étiquette pour afficher le résultat
    result_label = tk.Label(window, text="", font=("Arial", 12))
    result_label.pack(pady=5)

    # Créer un bouton pour valider l'expression
    validate_button = tk.Button(window, text="Valider", command=validate_and_evaluate, **btn_style)
    validate_button.pack(pady=5)

    # Créer un bouton "Retour" pour revenir aux calculatrices
    return_button = tk.Button(window, text="Retour", command=lambda: show_calculators(window), **btn_style)
    return_button.pack(pady=5)

    # Lier l'appui sur la touche Entrée à la fonction validate_and_evaluate
    window.bind('<Return>', lambda event: validate_and_evaluate())

def open_fibonacci_calculator(window, btn_style):
    """Affiche la calculatrice pour la suite Fibonacci."""
    global validate_button, return_button, lbl_start, entry_start, lbl_end, entry_end, result_label

    hide_buttons(all_buttons)  # Cacher les autres boutons

    # Utilisation des variables globales existantes
    lbl_start = tk.Label(window, text="Nombre de départ:", font=("Arial", 12))
    lbl_start.pack(pady=5)

    entry_start = tk.Entry(window, font=("Arial", 12), width=30)
    entry_start.insert(0, "0")  # Valeur de départ par défaut
    entry_start.pack(pady=5)

    lbl_end = tk.Label(window, text="Nombre de fin:", font=("Arial", 12))
    lbl_end.pack(pady=5)

    entry_end = tk.Entry(window, font=("Arial", 12), width=30)
    entry_end.pack(pady=5)

    result_label = tk.Label(window, text="", font=("Arial", 12))
    result_label.pack(pady=5)

    def validate_and_calculate():
        """Valider et calculer la suite Fibonacci."""
        start = entry_start.get()
        end = entry_end.get()

        # Vérifier si les entrées sont des nombres
        if not start.isdigit() or not end.isdigit():
            result_label.config(text="Veuillez entrer des nombres valides.")
            return

        # Convertir les entrées en entiers
        start = int(start)
        end = int(end)

        # Calculer la suite Fibonacci
        fib_sequence = [0, 1]  # Les deux premiers nombres de la suite Fibonacci

        while fib_sequence[-1] + fib_sequence[-2] <= end:
            next_fib = fib_sequence[-1] + fib_sequence[-2]
            fib_sequence.append(next_fib)

        # Filtrer les nombres dans l'intervalle spécifié
        fib_sequence = [num for num in fib_sequence if num >= start]

        # Afficher la suite Fibonacci
        result_label.config(text="Suite Fibonacci: " + ", ".join(map(str, fib_sequence)))

    # Créer le bouton Valider s'il n'existe pas
    if not validate_button:
        validate_button = tk.Button(window, text="Valider", command=validate_and_calculate, **btn_style)
    validate_button.pack(pady=5)

    # Créer le bouton Retour s'il n'existe pas
    if not return_button:
        return_button = tk.Button(window, text="Retour", command=lambda: show_calculators(window), **btn_style)
    return_button.pack(pady=5)

    # Lier l'appui sur la touche Entrée à la fonction validate_and_calculate
    window.bind('<Return>', lambda event: validate_and_calculate())




def open_compound_interest_calculator(window, btn_style):
    """Affiche la calculatrice pour l'intérêt composé avec dépôts."""
    global validate_button, return_button, result_label, principal_entry, rate_entry, time_entry, periods_entry, payment_amount_entry, lbl_principal, lbl_rate, lbl_time, lbl_periods, lbl_payments

    # Cacher les autres boutons et éléments
    hide_buttons(all_buttons)

    # Éléments de l'interface utilisateur
    lbl_principal = tk.Label(window, text="Montant principal:", font=("Arial", 12))
    lbl_principal.pack(pady=5)
    principal_entry = tk.Entry(window, font=("Arial", 12), width=30)
    principal_entry.pack(pady=5)

    lbl_rate = tk.Label(window, text="Taux d'intérêt annuel (%):", font=("Arial", 12))
    lbl_rate.pack(pady=5)
    rate_entry = tk.Entry(window, font=("Arial", 12), width=30)
    rate_entry.pack(pady=5)

    lbl_time = tk.Label(window, text="Durée (années):", font=("Arial", 12))
    lbl_time.pack(pady=5)
    time_entry = tk.Entry(window, font=("Arial", 12), width=30)
    time_entry.pack(pady=5)

    lbl_periods = tk.Label(window, text="Nombre de périodes par an:", font=("Arial", 12))
    lbl_periods.pack(pady=5)
    periods_entry = tk.Entry(window, font=("Arial", 12), width=30)
    periods_entry.pack(pady=5)
    periods_entry.insert(0, "12")  # Par défaut, calcul des intérêts une fois par mois

    lbl_payments = tk.Label(window, text="Montant des paiements périodiques:", font=("Arial", 12))
    lbl_payments.pack(pady=5)
    payment_amount_entry = tk.Entry(window, font=("Arial", 12), width=30)
    payment_amount_entry.pack(pady=5)
    payment_amount_entry.insert(0, "0")  # Valeur par défaut pour le montant des paiements périodiques

    result_label = tk.Label(window, text="", font=("Arial", 12))
    result_label.pack(pady=5)

    def validate_and_calculate():
        """Valider et calculer l'intérêt composé."""
        principal = float(principal_entry.get())
        rate = float(rate_entry.get()) / 100  # Convertir en décimal
        time = float(time_entry.get())
        periods_per_year = float(periods_entry.get())

        payment_amount = float(payment_amount_entry.get())

        # Calcul de l'intérêt composé avec paiements périodiques une fois par mois
        periods = time * periods_per_year  # Nombre total de périodes
        monthly_rate = rate / 12  # Taux d'intérêt mensuel
        amount = principal * (1 + monthly_rate) ** periods + payment_amount * ((1 + monthly_rate) ** (periods + 1) - 1) / monthly_rate
        interest = amount - principal - (payment_amount * periods)

        result_label.config(text=f"Montant total: {amount:.2f}, Intérêt total: {interest:.2f}")

    # Bouton pour valider le calcul
    validate_button = tk.Button(window, text="Valider", command=validate_and_calculate, **btn_style)
    validate_button.pack(pady=5)

    # Bouton pour retourner à la sélection de calculatrice
    return_button = tk.Button(window, text="Retour", command=lambda: show_calculators(window), **btn_style)
    return_button.pack(pady=5)

    # Lier l'appui sur la touche Entrée à la fonction validate_and_calculate
    window.bind('<Return>', lambda event: validate_and_calculate())







def open_unit_converter(window, btn_style):
    global input_field, validate_button, return_button, result_label, input_unit_dropdown, output_unit_dropdown

    # Cacher tous les boutons et éléments
    hide_buttons(all_buttons)

    # Créer un widget d'entrée (input) pour la valeur à convertir
    input_field = tk.Entry(window, font=("Arial", 12), width=30)
    input_field.pack(pady=10)

    # Créer un menu déroulant pour sélectionner l'unité d'entrée
    input_unit_options = ['Degrés', 'Mètres', 'Kilogrammes']
    input_unit_var = tk.StringVar(window)
    input_unit_var.set(input_unit_options[0])  # Définir la valeur par défaut
    input_unit_dropdown = tk.OptionMenu(window, input_unit_var, *input_unit_options)
    input_unit_dropdown.pack()

    # Créer un menu déroulant pour sélectionner l'unité de sortie
    output_unit_var = tk.StringVar(window)
    output_unit_dropdown = tk.OptionMenu(window, output_unit_var, '')
    output_unit_dropdown.pack()

    def update_output_dropdown(*args):
        selected_unit = input_unit_var.get()
        if selected_unit == 'Degrés':
            output_unit_options = ['Fahrenheit', 'Kelvin']
        elif selected_unit == 'Mètres':
            output_unit_options = ['Kilomètres', 'Hectomètres', 'Décamètres', 'Décimètres', 'Millimètres']
        elif selected_unit == 'Kilogrammes':
            output_unit_options = ['Grammes', 'Milligrammes']

        output_unit_var.set(output_unit_options[0])  # Définir la première option comme sélection par défaut
        # Mettre à jour les options du deuxième menu déroulant
        output_unit_dropdown['menu'].delete(0, 'end')
        for option in output_unit_options:
            output_unit_dropdown['menu'].add_command(label=option, command=tk._setit(output_unit_var, option))

    # Appeler la fonction pour mettre à jour les options initiales
    update_output_dropdown()

    # Ajouter un observateur (observer) pour le menu déroulant d'entrée afin de mettre à jour dynamiquement les options du deuxième menu déroulant
    input_unit_var.trace('w', update_output_dropdown)

    # Initialiser result_label
    result_label = tk.Label(window, text="", font=("Arial", 12))
    result_label.pack(pady=5)

    def validate_and_calculate():
        input_unit = input_unit_var.get()
        output_unit = output_unit_var.get()
        input_value = float(input_field.get())

        if input_unit == 'Degrés':
            if output_unit == 'Fahrenheit':
                result = (input_value * 9/5) + 32
            elif output_unit == 'Kelvin':
                result = input_value + 273.15
        elif input_unit == 'Mètres':
            if output_unit == 'Kilomètres':
                result = input_value / 1000
            elif output_unit == 'Hectomètres':
                result = input_value / 100
            elif output_unit == 'Décamètres':
                result = input_value / 10
            elif output_unit == 'Décimètres':
                result = input_value * 10
            elif output_unit == 'Millimètres':
                result = input_value * 1000
        elif input_unit == 'Kilogrammes':
            if output_unit == 'Grammes':
                result = input_value * 1000
            if output_unit == 'Milligrammes':
                result = input_value * 1000000

        result_label.config(text=f"{input_value} {input_unit} équivaut à {result} {output_unit}")

    # Bouton pour valider et convertir l'unité
    validate_button = tk.Button(window, text="Convertir", command=validate_and_calculate, **btn_style)
    validate_button.pack(pady=5)

    # Bouton "Retour" pour revenir à la sélection de calculatrices
    return_button = tk.Button(window, text="Retour", command=lambda: show_calculators(window), **btn_style)
    return_button.pack(pady=5)

    # Lier l'appui sur la touche Entrée à la fonction validate_and_calculate
    window.bind('<Return>', lambda event: validate_and_calculate())




def open_time_calculator(window, btn_style):
    global validate_button, return_button, start_date_default, end_date_default, lbl_start, entry_start, lbl_end, entry_end, lbl_years, lbl_months, lbl_weeks, lbl_days, lbl_hours, lbl_minutes, lbl_seconds

    # Cacher tous les boutons et éléments
    hide_buttons(all_buttons)

    # Calculer la date et l'heure d'il y a 7 jours
    start_date_default = datetime.now() - timedelta(days=7)
    end_date_default = datetime.now()

    # Créer un libellé et une entrée pour la date de début
    lbl_start = tk.Label(window, text="Date de début (YYYY-MM-DD HH:MM:SS):", font=("Arial", 12))
    lbl_start.pack(pady=5)
    entry_start = tk.Entry(window, font=("Arial", 12), width=30)
    entry_start.insert(0, start_date_default.strftime('%Y-%m-%d %H:%M:%S'))
    entry_start.pack(pady=5)

    # Créer un libellé et une entrée pour la date de fin
    lbl_end = tk.Label(window, text="Date de fin (YYYY-MM-DD HH:MM:SS):", font=("Arial", 12))
    lbl_end.pack(pady=5)
    entry_end = tk.Entry(window, font=("Arial", 12), width=30)
    entry_end.insert(0, end_date_default.strftime('%Y-%m-%d %H:%M:%S'))
    entry_end.pack(pady=5)

    # Créer des libellés pour afficher les résultats
    lbl_years = tk.Label(window, text="", font=("Arial", 12))
    lbl_years.pack()
    lbl_months = tk.Label(window, text="", font=("Arial", 12))
    lbl_months.pack()
    lbl_weeks = tk.Label(window, text="", font=("Arial", 12))
    lbl_weeks.pack()
    lbl_days = tk.Label(window, text="", font=("Arial", 12))
    lbl_days.pack()
    lbl_hours = tk.Label(window, text="", font=("Arial", 12))
    lbl_hours.pack()
    lbl_minutes = tk.Label(window, text="", font=("Arial", 12))
    lbl_minutes.pack()
    lbl_seconds = tk.Label(window, text="", font=("Arial", 12))
    lbl_seconds.pack()

    def calculate_time_difference():
        try:
            start_date = datetime.strptime(entry_start.get(), '%Y-%m-%d %H:%M:%S')
            end_date = datetime.strptime(entry_end.get(), '%Y-%m-%d %H:%M:%S')
            time_difference = end_date - start_date

            years = time_difference.days // 365
            months = time_difference.days // 30
            weeks = time_difference.days // 7
            days = time_difference.days
            hours = round(time_difference.seconds / 3600 + time_difference.days * 24)
            minutes = ((time_difference.seconds % 3600) // 60) + time_difference.days * 24 * 60
            seconds = time_difference.seconds + time_difference.days * 24 * 60 * 60

            lbl_years.config(text=f"Years: {years}")
            lbl_months.config(text=f"Months: {months}")
            lbl_weeks.config(text=f"Weeks: {weeks}")
            lbl_days.config(text=f"Days: {days}")
            lbl_hours.config(text=f"Hours: {hours}")
            lbl_minutes.config(text=f"Minutes: {minutes}")
            lbl_seconds.config(text=f"Seconds: {seconds}")
        except ValueError:
            lbl_years.config(text="Invalid date format")
            lbl_months.config(text="")
            lbl_weeks.config(text="")
            lbl_days.config(text="")
            lbl_hours.config(text="")
            lbl_minutes.config(text="")
            lbl_seconds.config(text="")

    # Bouton pour valider et convertir l'unité
    validate_button = tk.Button(window, text="Calculate", command=calculate_time_difference, **btn_style)
    validate_button.pack(pady=5)

    # Bouton pour revenir à la sélection de calculatrice
    return_button = tk.Button(window, text="Retour", command=lambda: show_calculators(window), **btn_style)
    return_button.pack(pady=5)

    # Lier l'appui sur la touche Entrée à la fonction calculate_time_difference
    window.bind('<Return>', lambda event: calculate_time_difference())


def open_imc_calculator(window, btn_style):
    global validate_button, return_button, label_poids, poids_entry, label_taille, taille_entry, result_label

    # Cacher tous les boutons et éléments
    hide_buttons(all_buttons)

    # Création des éléments de l'interface utilisateur
    label_poids = tk.Label(window, text="Entrez votre poids (kg):", font=("Arial", 12))
    label_poids.pack(pady=5)

    poids_entry = tk.Entry(window, font=("Arial", 12), width=30)
    poids_entry.pack(pady=5)

    label_taille = tk.Label(window, text="Entrez votre taille (m):", font=("Arial", 12))
    label_taille.pack(pady=5)

    taille_entry = tk.Entry(window, font=("Arial", 12), width=30)
    taille_entry.pack(pady=5)

    result_label = tk.Label(window, text="", font=("Arial", 12))
    result_label.pack(pady=5)

    # Définition de la fonction de calcul de l'IMC
    def calculate_imc():
        poids = float(poids_entry.get())
        taille = float(taille_entry.get())
        imc = poids / (taille * taille)
        result_label.config(text=f"Votre IMC est de : {imc:.2f}")

    # Bouton pour valider et calculer l'IMC
    validate_button = tk.Button(window, text="Calculer IMC", command=calculate_imc, **btn_style)
    validate_button.pack(pady=5)

    # Bouton "Retour" pour revenir à la sélection de calculatrice
    return_button = tk.Button(window, text="Retour", command=lambda: show_calculators(window), **btn_style)
    return_button.pack(pady=5)

    # Lier l'appui sur la touche Entrée à la fonction calculate_imc
    window.bind('<Return>', lambda event: calculate_imc())

def create_window():
    global all_buttons

    # Création de la fenêtre principale
    window = tk.Tk()
    window.title("Calculatrices")

    # Définition de la taille de la fenêtre
    window.geometry("700x1000")

    # Configuration du style
    window.configure(bg="#f0f0f0")

    def close_window(window):
        window.destroy()

    # Style pour les boutons
    btn_style = {"bg": "#007bff", "fg": "white", "font": ("Arial", 12), "width": 20, "height": 2, "bd": 0}

    # Boutons pour accéder aux différentes calculatrices
    all_buttons = []

    btn_standard = tk.Button(window, text="Calculatrice", command=lambda: open_standard_calculator(window, btn_style), **btn_style)
    btn_standard.pack(pady=5)
    all_buttons.append(btn_standard)

    btn_binary = tk.Button(window, text="Binaire", command=lambda: open_binary_calculator(window, btn_style), **btn_style)
    btn_binary.pack(pady=5)
    all_buttons.append(btn_binary)

    btn_fibonacci = tk.Button(window, text="Suite Fibonacci", command=lambda: open_fibonacci_calculator(window, btn_style), **btn_style)
    btn_fibonacci.pack(pady=5)
    all_buttons.append(btn_fibonacci)

    btn_interest = tk.Button(window, text="Intérêt Composé", command=lambda: open_compound_interest_calculator(window, btn_style), **btn_style)
    btn_interest.pack(pady=5)
    all_buttons.append(btn_interest)

    btn_conversion = tk.Button(window, text="Conversion", command=lambda: open_unit_converter(window, btn_style), **btn_style)
    btn_conversion.pack(pady=5)
    all_buttons.append(btn_conversion)

    btn_date = tk.Button(window, text="Date", command=lambda: open_time_calculator(window, btn_style), **btn_style)
    btn_date.pack(pady=5)
    all_buttons.append(btn_date)

    btn_imc = tk.Button(window, text="Calcul d'IMC", command=lambda: open_imc_calculator(window, btn_style), **btn_style)
    btn_imc.pack(pady=5)
    all_buttons.append(btn_imc)

    # Bouton pour fermer la fenêtre
    close_button = tk.Button(window, text="Fermer", command=lambda: close_window(window), **btn_style)
    close_button.pack(pady=5)
    all_buttons.append(close_button)

    # Lancement de la boucle principale pour afficher la fenêtre
    window.mainloop()

# Appel de la fonction pour créer la fenêtre
create_window()
