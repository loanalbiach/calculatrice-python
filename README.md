# Calculatrices Python

Ce projet consiste en une application de calculatrices réalisée en Python avec l'interface graphique Tkinter. L'application comprend plusieurs calculatrices pour effectuer divers types de calculs mathématiques et de conversions.

## Installation

Pour exécuter l'application, assurez-vous d'avoir Python installé sur votre système. Vous pouvez télécharger Python depuis le site officiel : [python.org](https://www.python.org/). Ensuite, vous pouvez cloner ce référentiel GitHub en utilisant la commande suivante dans votre terminal :

```
git clone https://gitlab.com/loanalbiach/calculatrice-python.git
```

Accédez au répertoire du projet :

```
cd calculatrice-python
```

Ensuite, exécutez le script `calculatrices.py` :

```
python calculatrice.py
```

## Fonctionnalités

L'application comprend les fonctionnalités suivantes :

- **Calculatrice standard :** Permet d'évaluer des expressions mathématiques simples.
- **Calculatrice binaire :** Convertit les nombres binaires entre différentes bases (binaire, décimale, hexadécimale).
- **Calculatrice de suite Fibonacci :** Génère la suite Fibonacci dans une plage spécifiée.
- **Calculatrice d'intérêt composé :** Calcule l'intérêt composé avec dépôts périodiques.
- **Convertisseur d'unités :** Convertit différentes unités de mesure (température, longueur, poids).
- **Calculatrice de durée :** Calcule la différence entre deux dates et heures spécifiées.
- **Calculatrice d'Indice de Masse Corporelle (IMC) :** Calcule l'IMC en fonction du poids et de la taille.

## Utilisation

Lorsque vous lancez l'application, une interface graphique s'ouvre avec plusieurs boutons correspondant à chaque calculatrice disponible. Vous pouvez sélectionner la calculatrice que vous souhaitez utiliser en cliquant sur le bouton correspondant.

Chaque calculatrice ouvre une nouvelle fenêtre où vous pouvez saisir les données nécessaires pour effectuer le calcul. Suivez les instructions spécifiques à chaque calculatrice pour obtenir les résultats souhaités.

*****************

Avec cette documentation, vous devriez être en mesure d'utiliser l'application de calculatrices Python de manière efficace. Si vous avez des questions ou des suggestions, n'hésitez pas à les partager !
